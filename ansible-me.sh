#!/bin/sh

cd || exit 1

if [ ! -e venv ]; then
    /usr/libexec/platform-python -m venv --system-site-packages venv
    . venv/bin/activate
    pip install --upgrade pip
    pip install ansible
else
    . venv/bin/activate
fi

cd ansible-desktop || exit 1

git pull

ansible-galaxy install -f -r requirements.yml
sudo setenforce 0 # make selinux permissive
ansible-playbook playbook.yml
sudo setenforce 1 # back again
