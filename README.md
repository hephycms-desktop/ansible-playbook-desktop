# HEPHY CMS Desktop

Automatic configuration of a desktop PC for HEPHYCMS

___A warning___: Take nothing for granted and keep always a backup
of important data.

Reboot the PC regularely to install various system updates. 

## Support

- For support with your desktop PC you can contact Dietrich.Liko@oeaw.ac.at

- For issues concerning the CLIP HPC computing facility, contact the [service desk](https://vbc.atlassian.net/servicedesk/customer/portal/5/group/6).

- For issues concerning the HEPHY infrastucture (printer, network, wlan, etc...), 
contact [HEPHY IT](hephy-it@oeaw.ac.at).

- For issues concerning the Austrian Academy of Science infrastructure contact the [helpdesk](helpdesk@oeaw.ac.at).

- For issues concerning the CERN infrastructure, contact the [service desk](service-desk@cern.ch) 

## Local Accounts on the PC

Standard Accounts:

    HEPHY Admin
    HEPHY Guest (Password hephy)

Regular user are supposed to add their own account and not to use the shared account.
After login with ```HEPHY Admin``` 

    sudo useradd <username> -c "<firstname> <lastname>'
    sudo password <username>

If you want to do your own system administration you can add the user to the wheel grouo

    sudo usermod <username> -a -G wheel

If you prefer another shell, you can set it from your account

    chsh -s /bin/zsh


## Accounts on other services

In addition to the local accounts you will have also accounts on remote systems. 

### Austrian Academy of Sciences

The basic account for email for members of the Austrian Academy of Sciences.
Usually the username is composed by a letter from your first name and 
your last name.

     aeinstein

You can change your [password](https://webloginx.oeaw.ac.at/adspw/index).

More information can be found at [OEAW Account](https://www.oeaw.ac.at/arz/services/account/oeaw-account) (German only)/

### CLIP (HPC computer)

The login for the HPC facility used for data analysis. Usually the account
name is given by

     albert.einstein

Login on various web based service requires to add a domain.

     albert.einstein@services.vbc.ac.at

The password can be changed after login with the command

    passwd

### CERN

The login at CERN. Usually the account name is given by your last name

    einstein

You can change your [password](https://account.cern.ch/account/Management/MyAccounts.aspx).

More information can be found at [CERN Account](https://cern.ch/account).

### HEPHY

The login account for local services of the institute:

    albert.einstein

For support you can contact the [HEPHY Office](hephy-office@oeaw.ac.at) or at the [HEPHY IT](mailto:hephy-it@oeaw.ac.at).

### Grid certificat

For usage of grid resources you need a personal X509 certificate. You can obtain it from 
[CERN](https://ca.cern.ch/ca/user/Request.aspx?template=EE2User). 

Access to CMS resources requires additional authorisation, which will happen during your 
CMS registration.

Details on how to configure the software to use the certificate are given in the 
[CMS Workbook](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookStartingGrid#BasicGrid) 
(or ask a collegue).

## Access to other systems using VPNs

Several services are behind firewalls and require a VPN to connect. VPN
can be configured interactively in the network control panel.

    go to Settings -> Network

Add a VPN by clicking on the plus sign.

### CLIP

- Select Multi-protocol VPN
- Choose a suited name (for example CLIP)
- For gateway enter: https://vpn.vbc.ac.at/AllSecure

You will be prompted your CLIP userid and password the first time you connect.

### HEPHY

- Work in Progress

### ÖAW

- Select Multi-protocol VPN
- Choose a suited name
- For gateway enter: gttps://wepvpn.oeaw.ac.at

## Usual remote accounts

### CLIP

After connecting to the VPN, the access to CLIP is via ```ssh```. CLIP uses
standard authentication, e.g. one can use ssh keys.

Create a .ssh directory, if it does not exists

Add following lines to .ssh/config

    Host clip cbe.vbc.ac.at
      Hostname cbe.vbc.ac.at
      User <clip username>
      ForwardAgent yes

Copy the ssh key from clip

     scp <clip user>@cbe.vbc.ac.at:.ssh/id_* .ssh/

The default private key on CLIP is unprotected. It is good praxis to
protect the key with a passphrase. The old passphrase is empty and
the new passphrase is like a password.

     ssh-keygen -p ~/.ssh/id_ecdsa

To menable the key, it has then to be loaded in the ssh-agent

     ssh-add

Now connection to clip should be without passwrd. As the key will
be forwarded, it can also be used to authenticate to github or to gitlab.

    ssh clip

### CERN lxplus

The CERN lxplus system is not protected by a VPN. Login wihout password
is possible using kerberos

First add following text to .ssh/config

    Host lxplus lxplus.cern.ch
        Hostname lxplus.cern.ch
        User <cern username>
        ForwardAgent yes
        GSSAPIAuthentication yes
        GSSAPIDelegateCredentials yes

Access can be granted by a kerberos ticket, with a time limited validity

    kinit -fp <cern user>@CERN.CH

Now connection to lxplus should be possible without password.

    ssh lxplus



## Remote Login to various systems

Remote login is based on ```ssh``` which allows to login
to remote computer in a secure way.

### CLIP

To access CLIP, it is required to enable first the VPN.

CLIP can be accessed using 

### lxplus

CERN is using kerberos authentication

### github.com


## EMail

The recommended mail client is thunderbird.

### ÖAW

### CERN

## Editors

It is my recommendation to use [Visual Studio Code](https://code.visualstudio.com). It supports local editing files on remote sites using a plugin. Get started by reading some of the [docs](https://code.visualstudio.com/docs)

I have found following plugins particular useful

* Remote-SSH
* Python
* ROOT File Viewer

There is also an assortment of other editors, as ```vi```, ```nano```, ```emacs``` and ```gedit```.

## CVMFS

The CERN VM Filesystem provides a global filesystem for the
distribution of software

## Access to EOS Storage Element

## Cloudbased Storage

### CERNBox

### ÖAWBox

### HEPHY Cloud

### Google Drive

## Virtualizations

### Singularity

[Singularity](https://sylabs.io) is a container system used for HPC computing. It support running images as a user
and can be used as a portable analysis environment. Docs can found [here](https://docs.sylabs.io/guides/latest/user-guide/)

### podman

[Podman](https://podman.io) is a implementation of OCI container supported by RedHat. It is compatible with Docker.
Similar to Singularity images can also be run in user mode.

## ROOT

## Conda

## Miscellaneous tools

### Bitwarden

### Chezmoi

## Installation for admins

Install Centos9 Stream with account HEPHY Admin

Install ansible structure

     curl -sL https://cern.ch/liko/ansible-me-setup.sh | bash -

and reboot.