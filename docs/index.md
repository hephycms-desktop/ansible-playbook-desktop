# Welcome

This is the ansible playbook to configure a desktop PC at HEPHY
for the CMS analysis group.

The documentation describe the intended usage of the installed
software.

## Installation

Install [Centos9 Stream](https://centos.org/stream9/) with account HEPHY Admin.

After the base installation is done, use this script to install the configuration.

     curl -sL https://cern.ch/liko/ansible-me-setup.sh | bash -

and reboot. Finally create a local accoubt for yourself:

    sudo useradd <username> -c "<firstname> <lastname>'
    sudo password <username>

If you need administraor rights ass yourself to the ```wheel``` group:

    sudo usermod –aG wheel UserName
    
## Maintainace

Reboot the PC regularely to install various system updates. This will also
updated software that is not pakaged as RPM.

## Feedback

Comments and requests on the [issue tracker](https://gitlab.cern.ch/hephycms-desktop/ansible-playbook-desktop/-/issues).
