# Communication

## EMail

The recommended mail client is thunderbird.

### ÖAW

If you need access to the email without configuration, use the webclient: https://exchange.oeaw.ac.at

The recommanded email client is thunderbird using the IMAP protocol. You have to configure
the account details manually.

Incoming

    Protocol: IMAP
    Hostname: imap.exchange.oeaw.ac.at
    Port: 993
    Connection security: SSL/TLS
    Authentication method: Normal Password
    Username: The OEAW login (aeinstein)

Outgoing email require also authentication with the SMTP server. You have to send a mail
to helpdesk@oeaw.ac.at to ask them to enable it.

Outgoing

    Hostname: smtp.oeaw.ac.at
    Port: 587
    Connection security: STARTTLS
    Authetication method: Normal Password
    Username: the OEAW login (aeinstein)

The official documentation by the Academy (german only): https://www.oeaw.ac.at/fileadmin/subsites/arz/pdf/sonstiges/anleitungen/imap.pdf

### CERN

If you need access to the email without configuration, use the webclient: https://mmm.cern.ch/owa/

To use IMAP access with CERN, you need to enable TLS 1.X in thunderbird (It is already enabled at OS level.)
Start thunderbird and select the menu at the rig=ght side of the window. Choose Settings. In Settuings scroll down 
in the the general menu and start the Config Editor. Enter ```security.tls.version.min```  and set its value to 1.

Again you should configure your account manually

Incoming:
   Protocol: IMAP
   Hostname: imap.cern.ch
   Port: 993
   Configuration security: SSL/TLS
   Authentication method: Normal password
   Username: You CERN login (einstein)

Outgoing:
    Hostname: smtp.cern.ch
    Port: 587
    Configuration security: STARTTLS
    Authentication method: Normal password
    Username: Your CERN login (einstain)


## Mattermost

CERN and also CMS is using Mattermost as communication tool.
The URL for the service is https://mattermost.web.cern.ch/

After configuring the server you will be prompted to enter your CERN 
login credentials.

## Zoom

The Zoom client is available and can be configured to use the CERN server. 
Use SSO as login method and enter ```cern.zoom.us``` as domain. You will be
prompted your CERN credentials

You can use also the instance of the Austrian Academy of Sciences ```oeaw-ac-at.zoom.us```. Here one can connect with the ÖAW account.


## MS Teams

MS team is the groupware of the Austrian Academy of Sciences. You can connect with your ÖAW Account (use the EMail and the password.)

## Skype

You have to use a private account for Skype.

## ÖAW Cloud

The Austrian Academy of Sciences  is providing a cloud service at http://oeawcloud.oeaw.ac.at. You have about 10 GB of disk space. The sync client is called nextcloud. Enter the URL and you will be asked for your OAW account.

## CERNBox

CERN is providing a cloud service at https://cernbox.cern.ch. You have 1 TB of diskspace availabkle. Currently the sync client is not available for CentOS 9.

## HEPHY Cloud

HEPHY is providing a cloud service at https://cloud01.hephy.at. You have 500 GB of diskspace available. The sync client - Sysnology Drive - is installed and can be configured with the HEPHY account.