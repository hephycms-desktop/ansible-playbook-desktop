# Accounts

Summary of user accounts and other authorisation issues for work with
the desktop and and on various clusters at HEPHY CMS.

## Local Accounts

The standard local accounts for the HEPHYCMS Desktop are.

    HEPHY Admin
    HEPHY Guest

User are strongly encouraged to add their own account and not to use the shared account
on a regular basis.

After login with ```HEPHY Admin``` 

    sudo useradd <username> -c "<firstname> <lastname>' -G wheel
    sudo password <username>

By adding the user to the wheel group, the user has the same sudo rights as HEPHY Admin.
System management task require the use of the ```sudo``` command.

## ÖAW

The [basic account](https://www.oeaw.ac.at/arz/services/account/oeaw-account) for members of 
the Austrian Academy of Sciences. Usually the username is composed by a letter from your 
first name and your last name.

     aeinstein

An incomplete list of services, that can be used with that account

- [EMail](https://exchange.oeaw.ac.at)
- [Eduroam WLAN](https://www.oeaw.ac.at/arz/services/datennetz/wlan-eduroam)
- [Time recording](https://zeit.oeaw.ac.at) (ÖAW VPN required)
- [ÖAW Cloud](https://oeawcloud.oeaw.ac.at)

The password can be changed in exchange or by a [web page](https://webloginx.oeaw.ac.at/adspw/index).

When changing the password, disconnect first all email clients on various devices. If an
old password is used by some software serveral times, the account will be locked for 15 mins and 
one can easily loose access. 

## CLIP (HPC computer)

The login for the HPC facility used for data analysis. Usually the account
name is given by

     albert.einstein

Login on various web based service requires to add the domain.

     albert.einstein@services.vbc.ac.at

An incomplete list of services, that can be used with that account. All services
require the use of the CLIP VPN).

- CLIP (login by ssh)
- [Jupytherhub](https://jupyterhub.vbc.ac.at)
- [Issue tracker](https://vbc.atlassian.net/servicedesk/customer/portal/5/group/6)

The password can be changed after ssh login using

    passwd

## CERN

The [login for CERN](https://account.cern.ch/account/). Usually the account name is given by your last name

    einstein

There are numerous services as 

 - lxplus (login by ssh)
 - [Gitlab](gitlab.cern.ch)
 - [CERNBox](https://cernbox.cern.ch)
 - [Helpdesk](https://cern.service-now.com/service-portal)

The password can be changed at [web page](https://account.cern.ch/account/CERNAccount/ChangePassword.aspx).

## HEPHY

The login account for local services of the institute:

    albert.einstein

## Access using VPNs

Several services are behind firewalls and require a VPN to connect. VPN
can be configured interactively in the network control panel.

    go to Settings -> Network

Add a VPN by clicking on the plus sign.

### CLIP

- Select Multi-protocol VPN
- Choose a suited name (for example CLIP)
- For gateway enter: https://vpn.vbc.ac.at/AllSecure

You will be prompted your CLIP userid and password the first time you connect.

### HEPHY

- Work in Progress

### ÖAW

- Select Multi-protocol VPN
- Choose a suited name
- For gateway enter: gttps://wepvpn.oeaw.ac.at

## Access without password

### CLIP

After connecting to the VPN, the access to CLIP is via ```ssh```. CLIP uses
standard authentication, e.g. one can use ssh keys.

Create a '''.ssh''' directory, if it does not exists

Add following lines to .ssh/config

    Host clip cbe.vbc.ac.at
      Hostname cbe.vbc.ac.at
      User <clip username>
      ForwardAgent yes

Copy the ssh key from clip

     scp <clip user>@cbe.vbc.ac.at:.ssh/id_* .ssh/

The default private key on CLIP is unprotected. It is good praxis to
protect the key with a passphrase. You can use following command
to set a new passphrase

     ssh-keygen -p ~/.ssh/id_ecdsa

To menable the key, it has then to be loaded in the ssh-agent

     ssh-add

Now connection to clip should pass be without passwrd. As the key will
be forwarded to the remote session, it can also be used to authenticate 
to the user to github or to gitlab.

    ssh clip

### CERN lxplus

The CERN lxplus system is not protected by a VPN. Login wihout password
is possible using kerberos

First add following text to .ssh/config

    Host lxplus lxplus.cern.ch
        Hostname lxplus.cern.ch
        User <cern username>
        ForwardAgent yes
        GSSAPIAuthentication yes
        GSSAPIDelegateCredentials yes

Access can be granted by a kerberos ticket, with a time limited validity

    kinit -fp <cern username>@CERN.CH

Now connection to lxplus should be possible without password.

    ssh lxplus

### Git repositories

#### github.com

One can also authenticate to github using the ssh-key. It is required that one is using
a ssh type url to close the repository.

Detail can  be found in the [github  documentation](https://docs.github.com/en/authentication/connecting-to-github-with-ssh)

#### gitlab.cern.ch

Also gitlab at CERN can be accessed with the ssh key - see the [gitlab documentation](https://docs.gitlab.com/ee/user/ssh.html)

### Grid certificate

You can obtain  a personal certificate from the [CERN Certification Authority](https://ca.cern.ch/ca/) (CA). Details
on how to get access to the CMS Grid resources can be found in  the [CMS workbook](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookStartingGrid)

The grid certificate can be installed locally on the desktop to directly access remote resources. It is also required
to install it on the remotre clusters (CLIP, CERN).
