#!/bin/sh

sudo useradd -m -r ansible -s /sbin/nologin

sudo sh -c "cat - > /etc/sudoers.d/ansible" <<EOF
ansible ALL=(root) NOPASSWD:ALL
Defaults:ansible !requiretty
EOF

sudo chmod 0440 /etc/sudoers.d/ansible

sudo sh -c "cat - > /etc/cron.d/ansible" <<EOF
@reboot ansible /home/ansible/ansible-desktop/ansible-me.sh &>>/var/log/ansible-me.log
EOF

sudo touch /var/log/ansible-me.log
sudo chown ansible /var/log/ansible-me.log

sudo dnf -y install git

sudo -u ansible git clone https://gitlab.cern.ch/hephycms-desktop/ansible-desktop.git /home/ansible/ansible-desktop 
